import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'muscula-angular-demo';

  onClickMe() {
    throw new Error('Demo error')
  }
}

import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {MusculaErrorHandler} from '../muscula/MusculaErrorHandler';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {MusculaHttpErrorInterceptor} from '../muscula/MusculaHttpInterceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],


  providers: [
    //Provide error Handler for Muscula
    {provide: ErrorHandler, useClass: MusculaErrorHandler},
    //Provide http error interceptor for Muscula
    {provide: HTTP_INTERCEPTORS, useClass: MusculaHttpErrorInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

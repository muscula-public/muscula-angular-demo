import {ErrorHandler, NgModule} from '@angular/core';
import * as MusculaLog from '@muscula.com/muscula-webapp-js-logger';

export class MusculaErrorHandler implements ErrorHandler {
  handleError(error: any) {
    console.log(error);
    //Report error to Muscula
    MusculaLog.Error(error.toString(), error);
  }
}


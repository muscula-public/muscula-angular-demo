import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as MusculaLog from '@muscula.com/muscula-webapp-js-logger';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

//initialize Muscula
MusculaLog.Init(ENTER_MUSCULA_ID);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

# MusculaAngularDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Muscula integration
1. Initialize Muscula in main.ts
2. Create MusculaErrorHandler, provide it in app.module.ts
3. Craete MusculaHttpInterceptor, provide it in app.module.ts
